---
title: ssh config
date: 2022-06-04
categories:
 - others
tags:
 - ssh
---

Thiết lập ssh từ ngoài vào server Ubuntu

### Cài đặt ssh server
```
$ sudo apt update
$ sudo apt install openssh-server
```

Thiết lập cấu hình cho ssh
```
# /etc/ssh/ssh_config
PermitRootLogin no
RSAAuthentication yes
PasswordAuthentication yes
PermitEmptyPasswords no
ChallengeResponseAuthentication no
```

Khởi động lại ssh service
```
$ systemctl restart sshd
```

### Thiết lập mạng
- Access vào [đây](https://www.cman.jp/network/support/go_access.cgi) Để biết được global IP đang sử dụng
- Cố định IP
```
# Address: 192.168.x.y => Local IP muốn cố định
# Netmask: 255.255.255.0
# Gateway: 192.168.x.1
# DNS: 192.168.10.1
```
- Thay đổi port trong /ect/service
- Thay đổi port trong /etc/ssh/ssh_config và restart lại
- Thay đổi port trong /etc/ssh/sshd_config và restart lại
```
$ sudo systemctl restart ssh
$ sudo systemctl restart sshd
```
- Mở port với tcp/udp
```
# Kiểm tra trạng thái hiện tại
$ sudo ufw status
# Thêm port với giao thức tcp
$ sudo ufww allow port/tcp
# Thêm port với giao thức udp
$ sudo ufww allow port/udp
# Reload lại ufw
$ sudo ufw reload
```

- Cấu hình cho router
  - Đăng nhập vào trang quản lý của router (Địa chỉ gateway IP, ví dụ như http://192.168.10.1/)
  - Tìm đến phần thiết lập firewall để thiết lập port forwarding
  ```
  # DestIP: Địa chỉ IP cố định ở trên
  # Source Port: Cổng muốn mở
  # Dest Port: Giống cổng trên
  # Protocol: TCP/UDP
  ```

### Kiểm thử kết nối
```
# Từ internal network
$ ssh -p port user_name@fixed_local_ip

# Từ external network
$ ssh -p port user_name@global_ip
# Chú ý là nếu test với external network thì ko thể dùng mạng nội bộ để test, phải kết nối vào một mạng khác
```

### Cài đặt port cho github, gitlab
Mặc định github và gitlab sẽ dùng cổng 22. Khi thay đổi côngr phía trên thì 2 dịch vụ trên sẽ không hoạt động được nữa. Vì vậy cần thêm cổng đã thiết lập vào cấu hình của github và gitlab như sau
```
Host github
  HostName github.com
  IdentityFile ~/.ssh/github_rsa
  Port [port]
  User git

Host gitlab
  HostName gitlab.com
  IdentityFile ~/.ssh/gitlab_rsa
  Port [port]
  User git
```

