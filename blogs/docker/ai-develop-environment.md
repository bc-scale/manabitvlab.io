---
title: Dùng hệ điều hành và môi trường gì để phát triển ứng dụng AI?
date: 2019-05-06
categories:
 - ai
tags:
 - ai
 - environment
 - docker
---
Trong việc học tập AI thì môi trường để viết code thử nghiệm, debug code rất quan trọng. 
Có một môi trường ổn định giúp bạn có hứng thú với việc học tập nghiên cứu hơn là việc cứ suốt 
ngày phải đi fix những lỗi không ổn định hoặc không tưởng thích thư viện.

## Chọn hệ điều hành
<p align="center">
  <img src="https://assets.ubuntu.com/v1/8dd99b80-ubuntu-logo14.png">
</p>

Giả sử các bạn có máy tính hoặc sắp mua máy tính thì nên chọn hệ điều hành nào cho phát triển các ứng dụng AI?
Mình cực kỳ khuyến khích các bạn chọn Ubuntu. Lý do thì rất nhiều
- Hệ điều hành mở -> khỏi lo việc mua license như Windows
- Update thường xuyên, nhất là những bản LTS (Long Time Support) như 18.04, 20.04
- Được sử dụng command line, sử dụng Vim mà không cần cài đặt lằng nhằng mới có như ở Windows
- Chạy cực kỳ ổn định với Docker, các framework phát triển AI như pytorch, tensorflow
- Thoải mái cài đặt phần mềm và phân quyền tùy thích theo ý muốn

Tuy nhiên, Ubuntu sẽ không phù hợp cho những bạn nào làm tài liệu dùng Microsoft Office. Mặc dù trên Ubuntu có open source LibreOffice nhưng nó cũng không thể nào tương thích và xịn xò như của MS mất phí.

Ngoài ra các bạn có thể dùng MacOS và Windows cũng được thôi, nhưng mình nói trước là việc cài đặt thư viện lên 2 hệ điều hành này thường sẽ không được support tốt như trên Ubuntu. Điều đó có nghĩa là cộng đồng phát triển AI ít người sử dụng Windows và MacOS, nếu bạn có gặp lỗi gì thì một là cố gắng mà tự sửa hoặc nếu may mắn thì có rất ít thông tin liên quan có thể giúp đỡ được bạn.

Sau khi chọn hệ điều hành thì tiếp theo sẽ là cài đặt gì lên đó để dựng môi trường?
## Sử dụng [Docker](https://docs.docker.com/get-docker/)
<p align="center">
  <img src="https://www.docker.com/sites/default/files/d8/2019-07/horizontal-logo-monochromatic-white.png">
</p>

Mình cực kỳ khyến khích mọi người sử dụng docker trong việc học tập và phát triển các ứng dụng AI.
Ưu điểm của docker thì rất nhiều, có thể liệt một vài ý mà mình thấy hay như sau
- Thay đổi version của môi trường dễ dàng, ví dụ có thể chuyển từ Ubuntu 16.04 lên thành 18.04 hoặc 20.04 với chỉ một dòng lệnh
- Dễ dàng cài đặt trên Ubuntu, Windows, MacOs, tài liệu hướng dẫn cũng rất đầy đủ cho từng loại OS
- Cài Docker vào sử dụng thì hầu như tận dụng được sức mạnh của phần cứng chứ không bị cản trở nhiều như các phần mềm virtual machine
- Khi không dùng nữa có thể xóa docker và docker container đi một cách dễ dàng. Việc này giúp cho máy tính của chúng ta luôn luôn sạch sẽ.
- Điểm lợi nữa là dùng docker khiến cho việc deploy sản phẩm hay demo một cách dễ dàng. Docker mô phỏng môi trường production, sau khi chạy thử nghiệm dưới local thì chúng ta chỉ việc đem docker image lên môi trường production, build trên đó một container rồi chạy như dưới local mà ít gặp trở ngại về vấn đề tương thích phần mềm. Có lẽ đây là một điểm lợi mà mình đánh giá cao nhất của Docker
- Ngoài ra việc dùng docker kết hợp với git sẽ giúp cho các thành viên trong team có một môi trường phát triển phần mềm thống nhất với nhau. Việc phát triển nhiều module song song trên nhiều máy tính khác nhau của các thành viên, và việc install các phần mềm phụ thuộc nếu không thông nhất version sẽ sinh ra những rắc rối khi chạy test thử nghiệm. Docker và git sẽ giúp giải quyết vấn đề này để tất cả các thành viên có một môi trường phát triển chung. Tránh thời gian lãng phí vào việc debug những lỗi không đáng có.
 
Để cài đặt docker các bạn tham khảo [bài viết này](https://manabitv.gitlab.io/blogs/docker/docker-install.html)

## Sử dụng [Anaconda](https://www.anaconda.com/)
Anaconda là một platform rất tốt và từ năm 2021 đã được thương mại hóa chứ không còn free như trước đó nữa.  
Anaconda giúp cho việc tạo ra các môi trường phát triển khác nhau trên máy tính, có thể switch qua lại giữa các môi trường cho từng project.  
Với mỗi môi trường độc lập thì có thể cài đặt tùy ý các thư viện, và tất nhiên khi không dùng nữa cũng có thể xóa đi nhanh chóng cho sạch máy.  
Anaconda về cơ bản tiện cho môi trường phát triển riêng từng cá nhân, còn nếu làm việc theo team thì sẽ không phù hợp. Mặc dù anaconda có support việc xuất môi trường ra file cài đặt rồi người khác có thể dựng lại môi trường đó. Tuy nhiên cách làm này vẫn còn nhiều lỗi khi dựng lại môi trường. Vì thế nó không phù hợp với làm việc theo team và cũng không phù hợp với việc deploy phần mềm lên môi trường production.

## Sử dụng [Pyenv Virtualenv](https://github.com/pyenv/pyenv-virtualenv)
Ngoài ra các bạn có thể sử dụng Pyenv Virtualenv.
Sử dụng Pyenv Virtualenv cũng có những điểm cộng và trừ như đối với sử dụng anaconda nên mình không bàn nhiều ở đây.

## Sử dụng [Google Colab](https://colab.research.google.com)
Câu chuyện rẽ sang một hướng khác, nếu bạn sử dụng google colab thì việc dùng máy tính gì, hệ điều hành gì chẳng còn quan trọng nữa. Bởi vì việc bạn cần chuẩn bị chỉ là một máy tính kết nối được internet, một tài khoản google email.  
Ngày trước mình cũng thử nghiệm dùng google colab. Nếu là sinh viên chưa có nhiều tiền đầu tư vào máy móc thì đây là một phương án khá okie. 
### Điểm cộng
Điểm cộng của nó là bật tắt môi trường đơn giản, có nhiều hướng dẫn sử dụng google colab nên các bạn search cũng sẽ ra rất nhiều. Gần đây google đã được thương mại hóa nữa nên sẽ được sự support technical từ phía google.  
### Điểm trừ
Có một vài điều mà mình không thích khi sử dụng google colab
- Thời gian sử dụng được ít: Nếu bạn chạy thử nghiệm mô hình hoặc code script nhỏ để debug, fix bug thì hoàn toàn okie. Nhưng nếu bạn chạy training mô hình lớn thì việc chỉ có 9 đến 12 tiếng là quá ít. Tất nhiên có cách là save mô hình xuống rồi lại load lên training lại, nhưng rõ ràng là nó sẽ tốn thời gian công sức của mình. Nhất là gần đây việc các máy free thường bị ngắt giữa chừng. Để giải quyết thì có phương án là mua Google Colab Pro, tầm 10$/tháng.
- Tốn thời gian cài đặt: Khi bật lên thì google sẽ cho bạn một instance mới tinh, có nghĩa là chỉ có vài thư viện có sẵn được cài đặt trên đó, còn hầu như các bạn sẽ phải cài đặt thêm nhiều mới chạy được thứ mình muốn. Nếu việc cài đặt này đơn giản thì chỉ cần chạy script là okie, tuy nhiên có nhiều thư viện mà mỗi lần cài đặt mất thời gian thì cách này không phải cách tối Ưu
- Thao tác với file khá là khó khăn vì phải thông qua google driver. Từ việc upload, tải file, hay show ra hình ảnh, video đều phải tuân theo chuẩn của google colab.
- Phát triển API cho ứng dụng AI: Giả sử bạn có model, bạn muốn viết API cho nó và thử nghiệm thì trên google colab có đáp ứng được không?
