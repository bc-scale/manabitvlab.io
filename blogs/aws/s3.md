---
title: Tìm hiểu về S3
date: 2021-10-26
categories:
 - aws
tags:
 - aws
---
Bài này sẽ giới thiệu về dịch vụ S3 của AWS
- S3 là gì?
- Các đặc điểm của S3
- S3 storage class
- S3 hoạt động như thế nào? (Bucket, Object)
- Cách access vào Amazon S3

## S3 là gì?
S3 viết tắt là Simple Storage Service. Là một dịch vụ cung cấp Object Storage không giới hạn dung lượng.

## Các đặc điểm của S3
- Được dùng để lưu trữ và backup dữ liệu
- Được access bằng https để tạo, tham chiếu, xóa dữ liệu
- Tự động được lưu trữ tại 3 Region (Data center), nên nếu chẳng may Available Zone có bị mất đi nữa thì dữ liệu vẫn được lưu trữ và đảm bảo tính khả dụng cao
- Độ bền lên đến 99.9% nên không bao giờ bị lo mất dữ liệu
- Số lượng Object trong Bucket là vô hạn, giới hạn của một Object là 5TB, một lần upload lên 5GB.
- Không có chức năng mount vào hệ thống dùng như NFS (Network File System)
- Một lần upload tối đa 5GB
- Một file giới hạn dung lượng là 5TB
- Áp dụng mô hình tính nhất quán (Nghĩa là khi dữ liệu đang thay đổi mà có người khác access vào thì sẽ chỉ nhìn thấy bản dữ liệu cũ). Điều đó có nghĩa, dữ liệu đang nhìn thấy có thể không phải là bản mới nhất.

## S3 storage class
|  Storage class  |  Ứng dụng  |
| ---- | ---- |
|  Standard  |  Loại data thường xuyên được access  |
|  Intelligent Tiering  | Loại access có partern thay đổi liên tục, không biết trước. Data được lưu trữ lâu dài   |
|  Standard-IA  |  Loại data không hay thường xuyên được access, lưu trữ lâu  |
|  1 Zone-IA  |  Loại data không hay thường xuyên được access, lưu trữ lâu, và data có tính không quan trọng  |
|  Glacier  |  Loại data được lấy ra dưới dạng archive trong khoảng từ vài phút đến vài giờ  |
|  Glacier Deep Archive  | Loại data được lấy ra dưới dạng archive trong khoảng dưới 12 tiếng  |
|  Reduced Redundancy  | Loại data có tính Redundancy thấp, tần suất access cao, data có tính không quan trọng  |


## Cách access vào Amazon S3
Có 3 cách
- AWS Management Console
- AWS CLI (command line)
- AWS SDK (program)

## S3 hoạt động như thế nào? (Bucket, Object, Key)

### Bucket
- Bucket ví như một chiếc hộp đựng dữ liệu
- Trong S3 thì nó là đơn vị lớn nhất
- Có thể tạo nhiều bucket cho từng mục đích sử dụng
- Có thể áp dụng nhiều quy định access cho từng bucket riêng
- Khi tạo bucket có thể chỉ định được region của nó

### Object
- Object được ám chỉ là dữ liệu khi upload
- Thứ được chứa bên trong bucket chính là object
- Object được xác định duy nhất bằng key và version của nó

### Key
- Là giá trị đại diện, và duy nhất cho mỗi object
- Mỗi một object sẽ có một key tương ứng

