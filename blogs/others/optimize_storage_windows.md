---
title: Tối ưu dung lượng ổ cứng khi dùng docker trên Windows
date: 2022-04-27
categories:
 - others
tags:
 - others
---
### Tối ưu dung lượng ổ cứng
```bash
# Khởi động powershell bằng quyền admin
$ Set-ExecutionPolicy -ExecutionPolicy Bypass
$ .\compact-wsl2-disk.ps1
$ Set-ExecutionPolicy -ExecutionPolicy Restricted
```
script được download từ [đây](https://github.com/mikemaccana/compact-wsl2-disk)