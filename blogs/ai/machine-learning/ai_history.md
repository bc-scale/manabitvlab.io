---
title: Lịch sử 60 năm của AI
date: 2021-10-06
categories:
 - ai
tags:
 - ai
---
Ngày nay đài báo truyền thông thường xuyên nhắc đến AI, AI đáng sợ, AI thật khủng khiếp, AI là công nghệ mới, đó là những gì xuất hiện trong đầu chúng ta khi nghe về nó. 
Tuy nhiên mọi người ít biết rằng, AI có một lịch sử nghiên cứu từ lâu, nó cũng từng bị hắt hủi để rồi trải qua 2 mùa đông lạnh giá.  
Trong bài viết này sẽ trình bày theo mốc thời gian để chúng ta hiểu được qua mỗi giai đoạn hình thành và phát triển, AI làm được gì và chưa làm được gì.

## Toàn cảnh lịch sử của AI 
Cho đến tận ngày nay, năm 2021 của thế kỷ 21 thì AI(Trí tuệ nhân tạo) một cách hoàn toàn vẫn chưa hề tồn tại. 
Có nhiều định nghĩa về AI, một AI hoàn toàn hoàn hảo như một máy tính có thể suy nghĩ được như con người.
Nó thực sự vẫn chưa tồn tại.

Làm sao để trí tuệ của con người được tái hiện trên máy tính là một chủ đề nghiên cứu rất nóng bỏng được nghiên cứu từ rất lâu. Kết quả vẫn chưa thành công.

![ai_history](/imgs/ai_history.png)

Hình vẽ bên trên mô tả cho chúng ta thấy được nghiên cứu AI được xen kẽ bởi thời kỳ đỉnh cao và thời kỳ băng giá.
- Làn sóng bùng nổ AI lần thứ nhất diễn ra từ những năm 1950 đến khoảng 1960. Thời kỳ này máy tính được lập trình để giải quyết những bài toán tìm kiếm và suy luận đơn giản
- Làn sóng bùng nổ AI lần thứ 2 là vào những năm 1980. Ở thời kỳ này, AI tập trung nghiên cứu làm sao để đưa tri thức vào trong máy tính.
- Làn sóng bùng nổ AI lần thứ 3 hiện nay, bắt đầu từ khoảng 2010. Sự bùng nổ của internet(những năm 2000) cũng chỉ là yếu tố chính là nên cách mạng AI lần thứ 3 này.

## Làn sóng bùng nổ AI lần thứ nhất
Trong thời kỳ này, những nhà nghiên cứu rất lạc quan về khả năng của AI, họ tập trung vào 2 chủ đề chính để nghiên cứu
- Suy luận: Chủ đề này tập trung vào việc dùng các ký hiệu để biểu diễn quy trình suy nghĩ của con người.
- Tìm kiếm: Phần này chủ yếu tập trung vào việc chia vấn đề thành các dạng trường hợp hợp cụ thể, sau đó thực hiện quá trình đi tìm lời giải đối với mỗi trường hợp đó. 
Ta có thể hình dung việc tìm đường cho mê cung là một loại bài này.

Khi con người giải mê cung, chúng ta dùng bút và hoặc tay để vẽ lên các hướng/đường có thể đi, tránh ra những ngõ cụt tìm ra con đường thoát khỏi mê cung.
Đôi với con người, khi số lượng các trường hợp lớn, mê cung phức tạp thì rất khó đưa ra được lời giải nhanh chính xác.
Máy tính thì khác, việc chia trường hợp ra, với trương hợp này thì giải quyết thế nào, trường hợp kia thì giải quyết làm sao. 
Khi có được lời giải cho từng trường hợp rồi thì máy tính cũng có thể tìm được ra kết quả chính xác như hoặc hơn con người và nhanh hơn con người.

Nhờ việc này mà máy tính có thể giải được những bài toán tìm đường khó, phức tạp trong thời gian ngắn.

### Hạn chế AI thời kỳ này
Tuy có thể giải quyết được những bài toán tìm đường nhanh, nhưng có một hạn chế là việc đưa ra các trường hợp để lập trình cho máy tình thì con người phải tự làm.
Trong cuộc sống thì những vấn đề chúng ta gặp không hề đơn giản. Giả sử để tăng doanh số bán hàng công ty thì sản phẩm phải như thế nào?
Hoặc muốn giảm cân thành công thì phải làm thế nào?
Khi đó, các nhà nghiên cứu nhận ra rằng, với kỹ thuật thời đó thì AI chỉ có thể giải quyết các bài 
toán đơn giản (toy problem), để giải quyết được những bài toán trong thực tế thì khó vô cùng. 
Bắt đầu niềm tin vào sự khả thi của AI không còn nữa, sự thất vọng lan rộng và kết quả là AI bước vào thời kỳ mùa đông lần 1.


## Bùng nổ trở lại lần 2
Thời kỳ băng giá kéo dài suốt cho tới khoảng 1980. ở đợt bùng nổ lần thứ 2 này, khái niệm tri thức được quan tâm và người ta nghiên cứu làm sao để đưa nó vào trong máy tính.
Người ta muốn đưa những kiến thức chuyên ngành về y tế vào thay cho các y bác sỹ trong việc khám chữa bệnh, hoặc muốn đưa những kiến thức về luật vào để máy tính làm thay công việc cho những nhà luật gia.

### Hệ chuyên gia (Expert system)
Thời kỳ này, mọi chú ý được đổ dồn vào thứ gọi là hệ chuyên gia.  
Hệ chuyên gia tức là  máy tính có thể trả lời được những câu hỏi có tính nghiệp vụ, chuyên môn cao như một chuyên gia. Để làm được những hệ này, người lập trình phải đưa lượng thông tin khổng lồ về lĩnh vực liên quan vào trong chương trình.
Nếu điều kiện X được thỏa mãn thì câu trả lời sẽ là YYYY. Cứ như thế, các câu hỏi và câu trả lời sẽ thành một cặp thông tin được đưa vào máy tính, giúp cho máy tính có thể trả lời các câu hỏi như một chuyên gia.  
Hệ chuyên gia tồn tại trong các ngành như y tế, công nghiệp, ngân hàng, tài chính hay nhân sự. Ở Mỹ, trong những năm 1980, nếu nói về AI thì người ta sẽ liên tưởng ngay đến các hệ chuyên gia này.

### Hạn chế của hệ chuyên gia
Hệ chuyên gia hoạt động tốt với những bài toán có yêu cầu rõ ráng. Tuy nhiên hệ chuyên gia cần hàng nghìn, hàng vạn rule để đưa vào lập trình, khó hơn nữa là khi số lượng rule ngày 
càng tăng thì để quản lý các rule không bị mâu thuẫn với nhau là một vấn đề khó.

Việc dùng hệ chuyên gia để thay y bác sỹ chuẩn đoán bệnh bắt đầu tỏ ra kém hiệu quả khi dữ liệu đưa vào không rõ ràng: cảm giác mệt mỏi, đầu nặng, bụng hơi râm ran đau. Đối với những dữ liệu đầu vào như thế, hệ chuyên gia không đưa ra được câu trả lời chính xác.
Và từ đó các nhà nghiên cứu bắt đầu nhận ra được sự hạn chế của AI thời kỳ này. Nó bước vào thời kỳ đông giá lần thứ 2.


## Bùng nổ lần thứ 3
Hiện tại, chúng ta đang ở trong thời kỳ bùng nổ lần 3 của AI. Một từ khóa được nhắc đến nhiều đó là học máy (Machine Learning).

### Sự ra đời của học máy (Machine Learning)
Học máy là một cơ chế AI tự học tập của máy móc. Ở đây học tập có thể hiểu là việc máy móc có thể tự động phân tách các trường hợp, hiểu biết và đưa ra các hành động. Những ví dụ đơn giản như việc máy tính có thể hiểu được object phía trước camera là quả táo hay không?
Hay có thể đưa ra khuyến nghị xem ngân hàng có nên cho một người A nào đó vay tiền hay không? hoặc nhà đầu tư có nên đầu tư vào doanh nghiệp B hay không. AI thời kỳ này có thể đưa ra những câu trả lời cho những tình huống thực sự trong đời sống như vậy.

Học máy là việc máy tính xử lý một lượng lớn dữ liệu trong quá khứ để nó học được việc làm sao phân chia trường hợp cho thích hợp nhất. Khi học được dữ liệu trong quá khứ, máy tính sẽ tự tính toán để đưa ra những phán đoán cho 
những thứ chưa hoàn toàn được học (nhưng có thuộc tính giống với những gì đã được học)

### Hạn chế của học máy
Tuy nhiên học máy cũng có điểm hạn chế. Đó là việc làm sao để thiết kế được các đặc tính của dữ liệu, nó như là đầu vào của học máy. Việc lựa chọn các đặc tính(feature) có ảnh hưởng đến rất nhiều đến độ chính xác của mô hình học máy, cụ thể hơn học máy có thể thực hiện được việc chia trường hợp được chính xác hơn.

Giả sử cần dự đoán thu nhập của một người. Như suy nghĩ bình thường, sẽ có các yếu tố ảnh hưởng như nơi sống, tuổi tác, ngành nghề. 
Còn những yếu tố như thích màu sắc gì, ngày sinh hoặc cầm tinh con gì thì chẳng liên quan gì đến thu nhập cao hay thấp. 
Tuy nhiên khi máy tính thực hiện, máy tính sẽ không thể hiểu đâu là yếu tố tốt hay yếu tố xấu ảnh hưởng kết quả dự đoán. Việc lựa chọn các yếu tố này đưa vào học máy con người phải làm thay cho máy tính.

### Sự ra đời của học sâu (Deep Learning)
Với sự ra đời của học sâu, máy tính có thể tự động trích xuất ra các đặc tính quan trọng, có giá trị từ trong dữ liệu mà không cần sự trợ giúp của con người. Đó là điểm khác biệt nhất giữa Deep Learning và Machine Learning.
Đối với Machine Learning, muốn nhấn diện quả táo đỏ thì còn người phải chỉ cho máy tính các đặc tính của quả táo, ví dụ như màu đỏ và hình tròn. Còn đối với Deep Learning, việc trích xuất những đặc điểm này là hoàn toàn tự động.
Tức là con người không cần phải chỉ ra trực tiếp thì mô hình trong máy tính cũng có thể lấy được các đặc tính đó.

Quá trình thực hiện trích xuất đặc tính này cực kỳ giống với quá trình xử lý trong não của cong người, vì vậy mà nó được gọi là neural network (mạng lưới thần kinh)

## Vấn đề của AI hiện tại là gì?
Vấn đề đầu tiên của Deep Learning đó các xử lý bên trong đều là black box (hộp đen). Con người thì nghĩ khi nhận diện quả táo sẽ dùng đặc tính màu đó, hình tròn. Nhưng khi máy tính thực hiện nhận diện có thể sử dụng thêm các đặc tính khác, đặc tính mà con người không thể hiểu được. Con người cũng không thể hiểu được tại sao Deep Learning lại sử dụng đặc tính đó.
Cũng chính bởi vì con người không hiểu bên trong nó xử lý sao, không nắm và control được logic bên trong nên sẽ nghi ngờ về tính kết quả của AI trả về. Người ta đặt ra những câu hỏi như liệu lái xe tự động có thực sự an toàn?
Giải thích được xử lý bên trong của AI sẽ giúp cho các doanh nghiệp mạnh dạn hơn ứng dụng AI vào trong thực tế.