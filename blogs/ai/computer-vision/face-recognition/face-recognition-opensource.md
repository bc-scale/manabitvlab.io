---
title: Face recognition opensource
date: 2021-11-06
categories:
 - face recognition
tags:
 - face recognition
---
Những opensource tốt nhất hiện nay cho face recognition

## [Deepface](https://github.com/serengil/deepface?ref=hackernoon.com)
- Hỗ trợ nhiều engine, phương pháp như Insightface, facenet
- Cung cấp REST API
- Dễ dùng
- Tuy nhiên chỉ cung cấp phương pháp để thực hiện face verification

## [Compareface](https://github.com/exadel-inc/CompreFace?ref=hackernoon.com)
- Khởi động chỉ với một lệnh dùng docker-compose
- Cung cấp REST API
- Tích hợp và mở rộng dễ dàng, phù hợp với những hệ thống streamvideo
- Cung cấp UI đơn giản để quản lý role và thu thập ảnh
- Cung cấp nhiều phương pháp nổi tiếng như FaceNet(99.65% trên LFW), Insightface(99.86% trên LFW)
- Đang trong quá trình phát triển

## [Face recognition](https://github.com/ageitgey/face_recognition?ref=hackernoon.com)
- Hỗ trợ docker
- Setup nhanh, dễ sử dụng
- Không cung cấp REST API
- Từ lâu rồi không được update
- Độ chính xác không cao (99.38% trên LFW)

## [Insightface](https://github.com/deepinsight/insightface?ref=hackernoon.com)
- Cung cấp solution cho nhận diện và detect mặt với độ chính xác cao
- Điểm trừ duy nhất của nó là khó cài đặt và sử dụng

## [FaceNet](https://github.com/davidsandberg/facenet?ref=hackernoon.com)
- Độ chính xác không bằng Insightface
- Không có REST API
- Từ 2018 không còn được update

## [Insightface-REST](https://github.com/SthPhoenix/InsightFace-REST?ref=hackernoon.com)
- Đang trong quá trình phát triển và đầy hứa hẹn
- Hỗ trợ docker
- Cung cấp REST API
- Nhanh hơn Insightface gấp 3 lần
- Điểm trừ là chỉ cung cấp API để thực hiện face embedding chứ không cung cấp API để thực hiện face recognition

## Tham khảo
- [Face Recognition Opensource](https://hackernoon.com/6-best-open-source-projects-for-real-time-face-recognition-vr1w34x5)
- [Tích hợp insightface lên web](https://zenn.dev/yuyakato/articles/c969e2818d266c)