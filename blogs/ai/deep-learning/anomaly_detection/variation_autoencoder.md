---
title: VAE (Variation AutoEncoder)
date: 2019-05-10
categories:
 - ai
tags:
 - anomaly detection
---
Gần đây trong thế giới Deep Learning, 2 từ khóa GAN và VAE trở thành từ khóa và công nghệ hot. Trong bài viết này sẽ giới thiệu thuật toán của VAE, sau đó sử dụng Pytorch để thực hành xây dựng mô hình.

Hiểu ngắn gọn thì VAE là một mô hình nén thông tin rồi phục hồi thông tin đó. Là một biến thể trong họ nhà AE (AutoEncoder).

## Mô hình VAE
Trong AE thì đơn giản chỉ là nén thông tin rồi khôi phục lại thông tin. Còn VAE là mô hình AE có sử dụng phân bố xác suất để tạo dữ liệu một cách 
có xác suất.
Chính vì vậy mà người ta còn nói VAE là một mô hình Generative Model. 
Mô hình này tạo ra một PDF(Probability Density Function) từ tập dữ liệu học.

## VAE khác gì với GAN?
Đặc điểm nổi bật của VAE chính là việc giảm được chiều dữ liệu (nén thông tin). 
Nói đến các kỹ thuật giảm chiều dữ liệu thì ta có thể liên tưởng tới PCA, SVD.
VAE nén chiều dữ liệu xuống thấp hơn trong một không gian Latent Space(Không gian ẩn), 
rồi lại tìm khôi phục dữ liệu đó(tất nhiên không thể khôi phục được hoàn toàn).
### Điểm cộng
- VAE khác PCA ở một điểm là trong không gian đã được giảm chiều đó thì các trục của các chiều không gian không cần phải vuông góc với nhau. 
- VAE hoạt động tốt trên cả các hàm phi tuyến tính phức tạp, tức là data không có quan hệ tuyến tính với nhau vẫn hoạt động tốt. 
Còn PCA thì chỉ hoạt động tốt trên các hàm tuyến tính.
- Các đặc trưng của PCA thường không có nhiều quan hệ với nhau, còn đặc trưng của VAE thì có nhiều quan hệ liên quan đến nhau. 
VÌ vậy mà khôi phục dữ liệu được tốt hơn.
### Điểm trừ
- VAE vì là AutoEncoder nên với cấu trúc phức tập có khả năng dẫn đến overfiting
- Chi phí tính toán của VAE nhiều hơn của PCA

Dưới đây là kết quả trên các tập tuyến tính/phi tuyến tính của 2 mô hình
<p align="center">
  <img src="https://miro.medium.com/max/1050/1*wciwhZ9fEL4RzHqCYSHh4w.png">
</p>

## VAE khác gì với AE?
VAE với AE thì cảm giác không khác nhau nhiều. Điểm khác biệt duy nhất là trong VAE có sử dụng xác suất.

Các điểm trong AE thì được mapping (chiếu) trực tiếp xuống một không gian ẩn (latent space) mà không cần tuân theo một phân bố xác suất nào.
Còn trong VAE, các điểm được chiếu xuống không gian ẩn theo một phân phối Gaus.
Khi decode, thì mạng Decoder sẽ lấy các giá trị z(đặc trưng của data đã được giảm chiều dữ liệu khi đi qua Encoder) một cách có xác suất.
Decoder cố gắng phục hồi dữ liệu cũ bằng những thông tin chứa trong z.

Vậy nên người ta cũng gọi VAE là một mô hình xác suất. Mô hình xác suất tức là, nếu data x tuân theo một phân phối, 
thì phân phối được thể hiện bằng một mô hình xác suất.

Giả sử x tuân theo phân phối chuẩn, thì phân phối xác suất của x chính là phân phối chuẩn, $P(x) = N(\mu, \sigma)$ là biểu diễn phân phối xác suất chuẩn của x.
Khi viết P(x) thì có thể gây hiểu nhầm P là hàm số, nhưng trong trường hợp này P là xác suất (Probability).

### Công thức toán học
Mục đích của VAE là mô hình hóa dữ liệu bằng mô hình xác suất.
Định nghĩa X là data và P(X) là phân bố xác suất của X, thì VAE sẽ đi tìm P(X) đó, tức là làm sao cực đại hóa được P(X).

P(X) được định nghĩa như sau

$$
P(X) = \int P(X|z)P(z)dz = \int P(X,z)
$$
