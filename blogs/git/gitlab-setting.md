---
title: Cài đặt gitlab ssh trên Windows
date: 2019-06-02
tags:
 - gitlab
---
Bình thường mình hay dùng Ubuntu để phát triển phần mềm. Nhưng đời không như mơ, nhiều lúc được vứt cho con máy Windows và bảo làm hết trên đó đi.
OK vậy là lại phải mò tìm cách setup môi trường cho nó. Vim, git, tmux chẳng hạn là những thứ đầu tiên mình settup khi nhận máy.

Bài này là những note lại của mình khi setup gitlab ssh cho Windows. Github chắc cũng tương tự hoặc dễ hơn, mình chưa thử nhé!

## Cài đặt git trên windows
Bước này không cần hướng dẫn nhiều, chỉ cần download từ [trang chủ này](https://git-scm.com/downloads) xuống rồi cài đặt như các phần mềm bình thường

## Setup tài khoản git trên máy
```bash
$ git config --global user.name "username"
$ git config --global user.email "email"
```

## Khởi tạo private key và public key
```bash
# di chuyển đến thư mục chứa key
$ cd ~/.ssh
$ ssh-keygen -t rsa -f gitlab_rsa
# view public key và copy public
$ cat gitlab_rsa.pub
```

## Setting public lên gitlab
Sau khi đã copy được public key bên trên, ta dán nó vào ssh key trên gitlab
`setting >> ssh key >> Tạo key và dán, setting thời gian hiệu lực của key >> lưu lại`

## Add keygen
Đăng ký private key vào key agent trên Windows
```bash
$ eval `ssh-agent`
$ ssh-add ~/.ssh/rsa_gitlab
``` 

## Tạo file config
```bash
User git
#Hostname gitlab.com
Host *
IdentityFile ~/.ssh/gitlab_rsa
TCPKeepAlive yes
IdentitiesOnly yes
```
Lưu file này tại `~/.ssh/config`

## Test
```bash
$ ssh -T git@gitlab
```

Nếu hiện ra kết quả đại loại như thì OK
```
Welcome to GitLab, @hoge!
```
